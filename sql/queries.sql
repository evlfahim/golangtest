-- queries.sql
-- name: CreateUser
INSERT INTO users (name, phone_number) VALUES ($1, $2) RETURNING id;

-- name: GenerateOTP
UPDATE users SET otp = $1, otp_expiration_time = $2 WHERE id = $3;

-- name: VerifyOTP
SELECT id, otp, otp_expiration_time FROM users WHERE phone_number = $1;
