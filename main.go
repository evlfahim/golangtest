package main

import (
	"context"
	"database/sql"
	"math/rand"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/jackc/pgx/v4/pgxpool"
)

const createUserQuery = "INSERT INTO users (name, phone_number) VALUES($1, $2) RETURNING id"
const generateOTPQuery = "UPDATE users SET otp = $1, otp_expiration_time = $2 WHERE id = $3"
const verifyOTPQuery = "SELECT id, otp, otp_expiration_time FROM users WHERE phone_number = $1"

type User struct {
	ID            int       `db:"id"`
	Name          string    `db:"name"`
	PhoneNumber   string    `db:"phone_number"`
	OTP           string    `db:"otp"`
	OTPExpiration time.Time `db:"otp_expiration_time"`
}

var db *pgxpool.Pool

func initDB() {
	// Connect to the PostgreSQL database
	poolConfig, err := pgxpool.ParseConfig("user=fahim password=password dbname=rest_api sslmode=disable")
	if err != nil {
		panic(err)
	}

	pool, err := pgxpool.ConnectConfig(context.Background(), poolConfig)
	if err != nil {
		panic(err)
	}

	db = pool
}

func createUser(c *gin.Context) {
	var user User

	// Parse JSON payload
	if err := c.ShouldBindJSON(&user); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	tx, err := db.Begin(context.Background())
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	defer tx.Rollback(context.Background())

	// Check if phone_number is unique
	var count int
	err = tx.QueryRow(context.Background(), "SELECT COUNT(*) FROM users WHERE phone_number = $1", user.PhoneNumber).Scan(&count)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	if count > 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Phone number already exists"})
		return
	}

	// Create a new user with the provided name and phone_number
	err = tx.QueryRow(context.Background(), createUserQuery, user.Name, user.PhoneNumber).Scan(&user.ID)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	if err := tx.Commit(context.Background()); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "User created"})
}

func generateOTP(c *gin.Context) {
	var userInput struct {
		PhoneNumber string `json:"phone_number"`
	}

	if err := c.ShouldBindJSON(&userInput); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Check if the phone number exists in the database
	var user User
	err := db.QueryRow(context.Background(), verifyOTPQuery, userInput.PhoneNumber).Scan(&user.ID, &user.OTP, &user.OTPExpiration)
	if err == sql.ErrNoRows {
		c.JSON(http.StatusNotFound, gin.H{"error": "Phone number not found"})
		return
	} else if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	// Generate a random 4-digit OTP
	rand.Seed(time.Now().UnixNano())
	generatedOTP := strconv.Itoa(1000 + rand.Intn(9000))

	// Set the OTP expiration time to 1 minute from the current time
	expirationTime := time.Now().Add(1 * time.Minute)

	tx, err := db.Begin(context.Background())
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	defer tx.Rollback(context.Background())

	_, err = tx.Exec(context.Background(), generateOTPQuery, generatedOTP, expirationTime, user.ID)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	if err := tx.Commit(context.Background()); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "OTP generated"})
}

func verifyOTP(c *gin.Context) {
	var otpInput struct {
		PhoneNumber string `json:"phone_number"`
		OTP         string `json:"otp"`
	}

	if err := c.ShouldBindJSON(&otpInput); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Check if the phone number and OTP combination is correct and not expired
	var user User
	err := db.QueryRow(context.Background(), verifyOTPQuery, otpInput.PhoneNumber).Scan(&user.ID, &user.OTP, &user.OTPExpiration)
	if err == sql.ErrNoRows {
		c.JSON(http.StatusNotFound, gin.H{"error": "Phone number not found"})
		return
	} else if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	if otpInput.OTP != user.OTP {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Incorrect OTP"})
		return
	}

	if time.Now().After(user.OTPExpiration) {
		c.JSON(http.StatusBadRequest, gin.H{"error": "OTP has expired"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "OTP verified"})
}

func main() {
	initDB()

	r := gin.Default()

	r.POST("/api/users", createUser)
	r.POST("/api/users/generateotp", generateOTP)
	r.POST("/api/users/verifyotp", verifyOTP)

	r.Run(":8080")
}
